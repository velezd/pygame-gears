import pygame, time

try:
    import android
except ImportError:
    android = None

# effects
class effect:
	# create effect
	def __init__(self, texture, speed, frames, scale = 1):
		self.frame = 0
		self.frames = 7
		self.speed = speed
		self.last = 0
		# prepare texture
		self.texture = pygame.transform.scale(texture, (texture.get_width() * scale, texture.get_height() * scale))
		self.width = self.texture.get_width()
		self.height = self.texture.get_height()
		
	# draw effect
	def update(self, lx, ly, rotation = 0):
		# animation
		if self.last+self.speed < ticks:
			self.frame = self.frame + 1
			self.last = ticks
			if self.frame == 7:
				self.frame = 0
		# create temp surface
		temp_surf = self.texture.subsurface((0, (self.height / self.frames) * self.frame, self.width, self.height/self.frames))
		# rotate
		if rotation != 0:
			temp_surf = pygame.transform.rotate(temp_surf, rotation)
		# draw effect and add update rect
		updates.append((lx, ly, temp_surf.get_width(), temp_surf.get_height()))
		screen.blit(temp_surf, (lx, ly))

def gears(gear256, gear128, gear64, text):
	rotated = pygame.transform.rotate(gear256, (x * 1)) # rotate gear
	updates.append((30, 30, 260, 260))
	screen.blit(rotated, (160-rotated.get_width()/2, 160-rotated.get_height()/2)) # put it to screen
	rotated = pygame.transform.rotate(gear128, -(x * 2))
	updates.append((310-65, 250-65, 132, 132))
	screen.blit(rotated, (310-rotated.get_width()/2, 250-rotated.get_height()/2))
	rotated = pygame.transform.rotate(gear64, (x * 4))
	updates.append((390-33, 220-33, 66, 66))
	screen.blit(rotated, (390-rotated.get_width()/2, 220-rotated.get_height()/2))
	textsurf = textfont.render(text, 1, (0,0,0)) # render text
	updates.append((100, 5, 340, 12)) # get position and size for update
	screen.blit(textsurf, (100,5)) # put it to screen
    
def main():
	# load config
	config = {}
	execfile('gears.conf', config)
	
	# start
	global textfont, screen, x, updates, ticks, clock
	
	game_width = config['width']
	game_height = config['height']
	
	pygame.init()
	pygame.font.init()
	
	clock = pygame.time.Clock()
	textfont = pygame.font.Font('./fonts/PressStart2P.ttf', 12)
	if config['fullscreen']:
		screen = pygame.display.set_mode((game_width, game_height), pygame.FULLSCREEN | pygame.HWSURFACE)
	else:
		screen = pygame.display.set_mode((game_width, game_height))
	
	if android:
		android.init()
		android.map_key(android.KEYCODE_BACK, pygame.K_ESCAPE)
		
	# loading images
	gear256a = pygame.image.load('./images/256.png')
	gear128a = pygame.image.load('./images/128.png')
	gear64a = pygame.image.load('./images/64.png')
	gear256k = pygame.image.load('./images/256_ck.png')
	gear128k = pygame.image.load('./images/128_ck.png')
	gear64k = pygame.image.load('./images/64_ck.png')
	shader = pygame.image.load('./images/shader.png')
	sparks = pygame.image.load('./images/sparks.png')
	
	if config['optlvl'] != 0:
		gear256a = gear256a.convert_alpha()
		gear128a = gear128a.convert_alpha()
		gear64a = gear64a.convert_alpha()
		gear256k = gear256k.convert()
		gear128k = gear128k.convert()
		gear64k = gear64k.convert()
		shader = shader.convert_alpha()
		sparks = sparks.convert_alpha()

	gear256k.set_colorkey((255, 0, 255))
	gear128k.set_colorkey((255, 0, 255))
	gear64k.set_colorkey((255, 0, 255))
	
	shader = pygame.transform.scale(shader, (game_width, game_height))

	if config['sparks']:
		spark = effect(sparks, 50, 7, 2)

	x = 0
	test = 0
	full_update = True
	while True:
		# getting events and time
		ev = pygame.event.poll()
		ticks = pygame.time.get_ticks()
		
		# android slepp
		if android:
			if android.check_pause():
				android.wait_for_resume()
				
		# animation and phases changing
		if x == 720:
			x = 0
			test = test + 1
			full_update = True
		if test > 1:
			test = 0
		x = x + 1

		# count fps
		clock.tick(1000)
		fps = textfont.render(str(round(clock.get_fps(), 1)), 1, (0,0,0))
		updates = [(5, 5, fps.get_width(), fps.get_height())]
		
		
		# benchmark phases
		if test == 0:
			screen.fill((255, 255, 255))
			gears(gear256a, gear128a, gear64a, 'Alpha channel')
			if config['vignetting']:
				screen.blit(shader, (0,0))
			if config['sparks']:
				spark.update(330, 200, 120)
			
		if test == 1:
			screen.fill((255, 255, 255))
			gears(gear256k, gear128k, gear64k, 'Color keying')
			if config['vignetting']:
				screen.blit(shader, (0,0))
			if config['sparks']:
				spark.update(330, 200, 120)
		
		# print fps
		screen.blit(fps, (5,5))
		
		# debug
		#for i in range(len(updates)):
		#	pygame.draw.rect(screen, (255,0,0), updates[i], 1)
		
		if full_update or config['optlvl'] != 2:
			pygame.display.update()
			full_update = False
		else:
			pygame.display.update(updates)

		# end app
		if ev.type == pygame.KEYDOWN and ev.key == pygame.K_ESCAPE:
			break

if __name__ == "__main__":
    main()
